<?php

/**
 * @file
 * Creates view dynamically for flag with feedback.
 */

/**
 * Implements hook_views_default_views().
 */
function flag_with_feedback_views_default_views() {
  $view = new view();
  $view->name = 'flag_stats';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'flag_with_feedback';
  $view->human_name = t('Flag stats');
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = t('Flag stats');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'response_text_feedback' => 'response_text_feedback',
    'flag' => 'flag',
    'fwfid' => 'fwfid',
    'timestamp' => 'timestamp',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'response_text_feedback' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'flag' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'fwfid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'timestamp' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No Results Found!';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Relationship: Flag with Feedback: Flagged Content */
  $handler->display->display_options['relationships']['entity_id']['id'] = 'entity_id';
  $handler->display->display_options['relationships']['entity_id']['table'] = 'flag_with_feedback';
  $handler->display->display_options['relationships']['entity_id']['field'] = 'entity_id';
  /* Field: Flag with Feedback: FWF ID */
  $handler->display->display_options['fields']['fwfid']['id'] = 'fwfid';
  $handler->display->display_options['fields']['fwfid']['table'] = 'flag_with_feedback';
  $handler->display->display_options['fields']['fwfid']['field'] = 'fwfid';
  /* Field: Flag with Feedback: Flag status */
  $handler->display->display_options['fields']['flag']['id'] = 'flag';
  $handler->display->display_options['fields']['flag']['table'] = 'flag_with_feedback';
  $handler->display->display_options['fields']['flag']['field'] = 'flag';
  /* Field: Flag with Feedback: Feedback */
  $handler->display->display_options['fields']['response_text_feedback']['id'] = 'response_text_feedback';
  $handler->display->display_options['fields']['response_text_feedback']['table'] = 'flag_with_feedback';
  $handler->display->display_options['fields']['response_text_feedback']['field'] = 'response_text_feedback';
  /* Field: Flag with Feedback: Updated timestamp */
  $handler->display->display_options['fields']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['table'] = 'flag_with_feedback';
  $handler->display->display_options['fields']['timestamp']['field'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['date_format'] = 'short';
  $handler->display->display_options['fields']['timestamp']['second_date_format'] = 'long';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['relationship'] = 'entity_id';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  // Add view to list of views to provide.
  $views[$view->name] = $view;
  // At the end, return array of default views.
  return $views;
}
