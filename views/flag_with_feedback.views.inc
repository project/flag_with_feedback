<?php

/**
 * @file
 * Using hooks_views_data to contruct view dynamically.
 */

/**
 * Implements hook_views_data().
 */
function flag_with_feedback_views_data() {
  $data['flag_with_feedback'] = array(
    'table' => array(
      'group' => 'Flag with Feedback',
      'base' => array(
        'field' => 'fwfid',
        'title' => 'Flag with Feedback Table',
        'help' => 'All the flags are saved in this table',
      ),
    ),
    'fwfid' => array(
      'title' => t('FWF ID'),
      'help' => t('ID field'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    'entity_id' => array(
      'title' => t('Entity Id'),
      'help' => t('Entity Id'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'relationship' => array(
        'handler' => 'views_handler_relationship',
        'base' => 'node',
        'base field' => 'nid',
        'title' => t('Flagged Content'),
        'label' => t('Flagged node content.'),
      ),
    ),
    'entity_type' => array(
      'title' => t('Entity type'),
      'help' => t('Entity type'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    'flag' => array(
      'title' => t('Flag status'),
      'help' => t('Whether the content is flagged or unflagged.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
    ),
    'response_text_feedback' => array(
      'title' => t('Feedback'),
      'help' => t('Flaging feedback'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
    ),
    'timestamp' => array(
      'title' => t('Updated Date'),
      'help' => t('Updated timestamp of the flag'),
      'field' => array(
        'click sortable' => TRUE,
        'handler' => 'views_handler_field_date',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_date',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
  );
  return $data;
}
