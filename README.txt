Flag With Feedback(FWF) is a flagging system to mark/flag the 
article (or any node) with feedback.

Features:
- FWF module allows the user to flag content 'Yes' or 'No'. In case of 'No',
 this module allows the user to give feedback on why its flagged as "No". 
- The feedback for each node can be viewed in tabs based on the user permission. 
- Easy to configure the content types and other FWF settings through admin UI.

Although we inspired from flag module, this module is not a sub-module of flag
or replacement for flag.

Dependencies: views

Installation: Just like any other drupal module, download and 
place the module in the module directory and install it.
