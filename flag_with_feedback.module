<?php

/**
 * @file
 * Flag with feedback module for flagging with feedback.
 *
 * Flag with feedback is to flag the nodes and also provide feedback form
 * with flag stats for individual node pages.
 */

/**
 * Implements hook_help().
 */
function flag_with_feedback_help($path, $arg) {
  switch ($path) {
    case 'admin/help#flag_with_feedback':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Flag With Feedback(FWF) is a flagging system to mark/flag the article (or any node) with feedback.') . '</p>';
      $output .= '<h3>' . t('Features') . '</h3>';
      $output .= '<dl>';
      $output .= '<dd>' . t('FWF module allows the user to flag content "Yes" or "No". In case of "No", this module allows the user to give feedback on why its flagged as "No".') . '</dt>';
      $output .= '<dd>' . t('The feedback for each node can be viewed in tabs based on the user permission') . '</dd>';
      $output .= '<dd>' . t('Easy to configure the content types and other FWF settings through admin UI.') . '</dd>';
      $output .= '</dl>';
      return $output;
  }
}

/**
 * Implements hook_menu().
 */
function flag_with_feedback_menu() {
  $items['admin/config/fwf'] = array(
    'title' => 'Flag with Feedback Settings',
    'description' => 'Settings related to fwf module.',
    'position' => 'right',
    'weight' => 0,
    'access arguments' => array('administer site configuration'),
  );
  $items['admin/config/fwf/fwf-settings'] = array(
    'title' => 'FWF Settings',
    'description' => 'Configuration settings for FWF widget',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('flag_with_feedback_settings_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['node/%node/fwf-stats'] = array(
    'title' => 'FWF Stats',
    'page callback' => 'flag_with_feedback_stats',
    'page arguments' => array(1),
    'access callback' => array('flag_with_feedback_stats_check'),
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * Function for access callback.
 */
function flag_with_feedback_stats_check() {
  $fwf_show_stats = variable_get('fwf_show_stats', array());
  global $user;
  $user_roles = $user->roles;
  $access = FALSE;

  $fwf_show_stats = array_values($fwf_show_stats);

  // User role check to show FWF stats menu.
  foreach ($user_roles as $rid => $user_role) {
    if (in_array($rid, $fwf_show_stats)) {
      $access = TRUE;
      // Content type check to show FWF stats menu.
      if (arg(0) == 'node' && is_numeric(arg(1))) {
        $node = node_load(arg(1));
        $fwf_allowed_content_types = variable_get('fwf_allowed_content_type', array());
        if (count($fwf_allowed_content_types) > 0) {
          if ($fwf_allowed_content_types[$node->type]) {
            $access = TRUE;
          }
          else {
            $access = FALSE;
          }
        }
      }
      break;
    }
  }

  return $access;
}

/**
 * Callback function for FWF Stats.
 *
 * @url: node/%node/fwf-stats
 */
function flag_with_feedback_stats($nid) {
  $output = views_embed_view('flag_stats', 'block');
  return $output;
}

/**
 * Callback function for FWF Settings.
 *
 * @url: admin/config/fwf/fwf-settings
 */
function flag_with_feedback_settings_form() {
  $flag_header_text = variable_get('fwf_header_text', '');
  $form['fwf_header_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Flag header text'),
    '#description' => t('The flag header text appears before the yes/no flag with feedback widget.'),
    '#default_value' => $flag_header_text,
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => FALSE,
  );

  $flag_yes_text_value = variable_get('fwf_yes_text', t('Yes'));

  $form['fwf_yes_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Flag yes button text'),
    '#default_value' => $flag_yes_text_value,
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $flag_no_text_value = variable_get('fwf_no_text', t('No'));

  $form['fwf_no_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Flag no button text'),
    '#default_value' => $flag_no_text_value,
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $flag_yes_message = variable_get('fwf_yes_message', t('Thanks for your feedback!'));

  $form['fwf_yes_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Flag yes button message'),
    '#description' => t('Message displayed after flagging the entity.'),
    '#default_value' => $flag_yes_message,
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => FALSE,
  );

  $flag_no_message = variable_get('fwf_no_message', t('Thanks for your feedback!'));
  $form['fwf_no_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Flag no button message'),
    '#description' => t('Message displayed after unflagging the entity.'),
    '#default_value' => $flag_no_message,
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => FALSE,
  );

  $flag_feedback_title = variable_get('fwf_feedback_title', t('Tell us more..'));
  $form['fwf_feedback_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Flag Feedback Title'),
    '#description' => t('Message to be displayed as title for the feedback form.'),
    '#default_value' => $flag_feedback_title,
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => FALSE,
  );

  $allowed_content_types = array();
  $content_types = node_type_get_types();
  foreach ($content_types as $content_type) {
    $machine_name = $content_type->type;
    $content_type_name = $content_type->name;
    $allowed_content_types[$machine_name] = $content_type_name;
  }
  $fwf_allowed_content_type = variable_get('fwf_allowed_content_type', array());
  $form['fwf_allowed_content_type'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allowed content type'),
    '#default_value' => $fwf_allowed_content_type,
    '#options' => $allowed_content_types,
    '#description' => '',
    '#multiple' => TRUE,
    '#required' => TRUE,
    '#weight' => 15,
  );

  $roles = user_roles();
  $fwf_show_stats = variable_get('fwf_show_stats', array());
  $form['fwf_show_stats'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Show stats for specific roles'),
    '#default_value' => $fwf_show_stats,
    '#options' => $roles,
    '#description' => t('Use this option to show/hide the flag stats while viewing the node'),
    '#multiple' => TRUE,
    '#required' => TRUE,
    '#weight' => 15,
  );

  return system_settings_form($form);
}

/**
 * Implements hook_node_view().
 */
function flag_with_feedback_node_view($node, $view_mode, $langcode) {
  $fwf_allowed_content_types = variable_get('fwf_allowed_content_type', array());

  if (count($fwf_allowed_content_types) > 0
  && $fwf_allowed_content_types[$node->type]) {
    $args = array('entity_id' => $node->nid, 'entity_type' => 'node');
    $fwf_form = drupal_get_form('flag_with_feedback_form', $args);

    $node->content['fwf_field'] = array(
      '#markup' => drupal_render($fwf_form),
      '#weight' => 1000,
    );
  }
}

/**
 * Flag with Feedback widget form.
 */
function flag_with_feedback_form($form, &$form_state) {

  if (isset($form_state['no_selected']) && $form_state['no_selected'] === TRUE) {
    $flag_feedback_title = variable_get('fwf_feedback_title', t('Tell us more..'));
    $form['comment'] = array(
      '#prefix' => '<div id="flag_with_feedback_form_wrapper"><div class="fwf_feedback_form">',
      '#suffix' => '</div>',
      '#type' => 'textarea',
      '#title' => $flag_feedback_title,
      '#default_value' => '',
      '#attributes' => array('autocomplete' => 'off'),
    );
    $form['submit-feedback'] = array(
      '#type' => 'submit',
      '#value' => 'Submit',
      '#ajax' => array(
        'callback' => 'flag_with_feedback_form_callback_no_comment_submit',
        'wrapper' => 'flag_with_feedback_form_wrapper',
      ),
      '#suffix' => '</div>',
    );
  }
  elseif (isset($form_state['yes_selected']) && $form_state['yes_selected'] === TRUE) {
    $form['flag_response_text'] = array(
      '#type' => 'markup',
      '#markup' => variable_get('fwf_yes_message', t('Thanks for your feedback!')),
    );
  }
  elseif (isset($form_state['no_selected_feedback']) && $form_state['no_selected_feedback'] === TRUE) {
    $form['flag_response_text'] = array(
      '#type' => 'markup',
      '#markup' => variable_get('fwf_no_message', t('Thanks for your feedback!')),
    );
  }
  else {
    $flag_header_text = variable_get('fwf_header_text', '');
    $form['header_text'] = array(
      '#prefix' => '<div id="flag_with_feedback_form_wrapper"><div class="fwf_header_text">',
      '#suffix' => '</div>',
      '#type' => 'markup',
      '#markup' => $flag_header_text,
    );
    $flag_yes_text_value = variable_get('fwf_yes_text', t('Yes'));
    $form['submit-yes'] = array(
      '#type' => 'submit',
      '#value' => $flag_yes_text_value,
      '#ajax' => array(
        'callback' => 'flag_with_feedback_form_callback_yes',
        'wrapper' => 'flag_with_feedback_form_wrapper',
      ),
    );
    $flag_no_text_value = variable_get('fwf_no_text', t('No'));
    $form['submit-no'] = array(
      '#type' => 'submit',
      '#value' => $flag_no_text_value,
      '#ajax' => array(
        'callback' => 'flag_with_feedback_form_callback_no',
        'wrapper' => 'flag_with_feedback_form_wrapper',
      ),
      '#suffix' => '</div>',
    );
  }

  return $form;
}

/**
 * Callback function for yes.
 */
function flag_with_feedback_form_callback_yes($form, $form_state) {
  $entity_id = $form_state['build_info']['args'][0]['entity_id'];
  $entity_type = $form_state['build_info']['args'][0]['entity_type'];

  unset($form_state['no_selected']);
  unset($form_state['fwf_id']);

  $form_state['yes_selected'] = TRUE;

  $form['comment']['#default_value'] = "";
  $form['comment']['#attributes'] = array('autocomplete' => 'off', 'placeholder' => t("Write your comment"));

  $fields = array(
    'entity_type' => $entity_type,
    'entity_id' => $entity_id,
    'flag' => 0,
    'timestamp' => time(),
  );
  db_insert('flag_with_feedback')
    ->fields($fields)
    ->execute();

  return drupal_rebuild_form('flag_with_feedback_form', $form_state, $form);
}

/**
 * Callback function for no.
 */
function flag_with_feedback_form_callback_no($form, $form_state) {
  $form_state['no_selected'] = TRUE;
  unset($form_state['yes_selected']);

  $entity_id = $form_state['build_info']['args'][0]['entity_id'];
  $entity_type = $form_state['build_info']['args'][0]['entity_type'];

  $fields = array(
    'entity_type' => $entity_type,
    'entity_id' => $entity_id,
    'flag' => 1,
    'timestamp' => time(),
  );
  $fwf_id = db_insert('flag_with_feedback')
    ->fields($fields)
    ->execute();

  $form_state['fwf_id'] = $fwf_id;

  $form['comment']['#default_value'] = "";
  $form['comment']['#attributes'] = array('autocomplete' => 'off', 'placeholder' => t("Write your comment"));

  return drupal_rebuild_form('flag_with_feedback_form', $form_state, $form);
}

/**
 * Callback function for feedback submit.
 */
function flag_with_feedback_form_callback_no_comment_submit($form, $form_state) {
  $fwf_id = $form_state['fwf_id'];

  $feedback = $form_state['values']['comment'];
  $fields = array(
    'response_text_feedback' => $feedback,
    'timestamp' => time(),
  );
  db_update('flag_with_feedback')
    ->fields($fields)
    ->condition('fwfid', $fwf_id, '=')
    ->execute();

  unset($form_state['no_selected']);
  unset($form_state['fwf_id']);
  $form_state['no_selected_feedback'] = TRUE;

  $form['comment']['#default_value'] = "";
  $form['comment']['#attributes'] = array('autocomplete' => 'off', 'placeholder' => t("Write your comment"));

  return drupal_rebuild_form('flag_with_feedback_form', $form_state, $form);
}

/**
 * Implements hook_views_api().
 */
function flag_with_feedback_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'flag_with_feedback') . '/views',
  );
}
